{
  imports = [
    ./dev
    ./hardware
    ./media
    ./theming
    ./xdg-mime.nix
  ];
}
