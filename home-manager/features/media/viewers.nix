{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.media.viewers.enable =
      lib.mkEnableOption "media viewers";
  };

  config = lib.mkIf config.homeMods.media.viewers.enable {
    home.packages = with pkgs; [
      feh
      floorp
      mpv
      pcmanfm
      qpwgraph
      screenkey
      steam
      zathura
    ];
  };

}
