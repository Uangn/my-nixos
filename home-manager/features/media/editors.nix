{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.media.editors.enable =
      lib.mkEnableOption "media editors";
  };

  config = lib.mkIf config.homeMods.media.editors.enable {
    home.packages = with pkgs; [
      ffmpeg
      imagemagick
      krita
      onlyoffice-bin
      silicon
      yt-dlp
    ];
  };

}
