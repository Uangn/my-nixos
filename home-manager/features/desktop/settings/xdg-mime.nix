{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.desktop.settings.xdg-mime.enable =
      lib.mkEnableOption "desktop settings xdg-mime";
  };

  config = lib.mkIf config.homeMods.desktop.settings.xdg-mime.enable {
    xdg.mimeApps = {
      enable = true;
      associations.added = {
        "application/pdf" = ["org.pwmt.zathura.desktop"];
        "inode/directory" = ["pcmanfm.desktop"];
      };

      defaultApplications = {
        "application/pdf" = ["org.pwmt.zathura.desktop"];
        "inode/directory" = ["pcmanfm.desktop"];
      };
    };
  };

}
