{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.dev.editors.nvim.enable =
      lib.mkEnableOption "dev editors nvim";
  };

  config = lib.mkIf config.homeMods.dev.editors.nvim.enable {
    programs.neovim = {
      enable = true;

      # extraLuaConfig = lib.fileContents "../nvim/init.lua";
    };

    programs.btop = {
      enable = true;
      settings = {
        color_theme = "tokyonight";
        vim_keys = true;
      };
    };

    home.shellAliases = {
      ll = "ls -l";
      la = "ls -la";
      ".." = "cd ..";

      v = "nvim";
      zz = "~/.local/bin/scripts/tmux-sessionizer";
      rr = ". ranger";
      cs = "xclip -selection clipboard";
      ci = "xclip -selection clipboard -t image/png -o";

      g = "git";
      ga = "git add";
      gc = "git commit";
      gs = "git status";
      gd = "git diff";
      gp = "git pull";
      gitp = "git push";

      nmcli-wifi-list = "nmcli d w list";
      nmcli-wifi-con = "nmcli d w c --ask";
      nmcli-wifi-discon = "nmcli c d";
      nmcli-wifi-off = "nmcli r w off";

      tl = "tmux ls";
      tn = "tmux new -s \${PWD##*/}";

      nxSwitch = "sudo nixos-rebuild switch --flake .#${userConfig.hostname}";
      nxSwitchDebug = "sudo nixos-rebuild switch --flake .#${userConfig.hostname} --option eval-cache false --show-trace";
      hmSwitch = "home-manager switch --flake .#${userConfig.username}@${userConfig.hostname}";
      hmSwitchDebug = "home-manager switch --flake .#${userConfig.username}@${userConfig.hostname} --option eval-cache false --show-trace";
    };

    home.sessionVariables = {
      EDITOR = "nvim";
      VISUAL = "nvim";
      TERMINAL = "alacritty";
    };

    programs.bash = {
      enable = true;

      bashrcExtra = ''
        ta() { if ! tmux a; then tmux new -s \$\{PWD##*/}; fi };
        lc () {
          # `command` is needed in case `lfcd` is aliased to `lf`
          cd "$(command lf -print-last-dir "$@")"
        }
      '';

      historyIgnore = [ "ls" "cd" "exit" ];
    };

    programs.zoxide = {
      enable = true;
      enableBashIntegration = true;
    };

    programs = {

      powerline-go = {
        enable = false;

        # newline = true;
        # pathAliases = {
        #   # "\\~/projects" = "pj:";
        # };

        extraUpdatePS1 = ''
          PS1=$PS1"\n";
        '';

        settings = {
          hostname-only-if-ssh = true;
          numeric-exit-codes = true;
          condensed = true;
          cwd-max-depth = 7;
          cwd-mode = "plain";
        };
      };

      starship = {
        enable = true;
      };

    };

    home.packages = with pkgs; [
      unstable.alacritty
      bat
      cargo
      cava
      cmus
      fzf
      gcc
      git
      lf
      ripgrep
      tldr
      tmux
      trash-cli
      unar
      unzip
      wget
    ];


    programs.git = {
      enable = true;
      #userName  = "";
      #userEmail = "";

      extraConfig = {
        filter.gitignore.clean = "sed '/###gitignore$/d'";
      };
    };
  };

}
