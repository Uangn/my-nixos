{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.hardware.screens.enable =
      lib.mkEnableOption "hardware screens";
  };

  config = lib.mkIf config.homeMods.hardware.screens.enable {
    services.autorandr.enable = true;
    programs.autorandr.enable = true;

    home.packages = with pkgs; [
      arandr
    ];
  };

}
