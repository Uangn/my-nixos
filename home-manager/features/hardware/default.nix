{
  imports = [
    ./control
    ./peripheral.nix
    ./screens.nix
  ];
}
