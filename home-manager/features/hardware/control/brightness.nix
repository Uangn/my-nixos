{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.hardware.control.brightness.enable =
      lib.mkEnableOption "control brightness";
  };

  config = lib.mkIf config.homeMods.hardware.control.brightness.enable {
    home.packages = with pkgs; [
      pavucontrol
      pulseaudio
      brightnessctl
    ];
  };

}
