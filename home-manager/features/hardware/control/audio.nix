{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.hardware.control.audio.enable =
      lib.mkEnableOption "control audio";
  };

  config = lib.mkIf config.homeMods.hardware.control.audio.enable {
    home.packages = with pkgs; [
      pavucontrol
      pulseaudio
    ];
  };

}
