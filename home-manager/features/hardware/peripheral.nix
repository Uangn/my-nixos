{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.hardware.peripheral.enable =
      lib.mkEnableOption "hardware peripheral";
  };

  config = lib.mkIf config.homeMods.hardware.peripheral.enable {
    home.packages = with pkgs; [
      openrgb
      piper
      solaar
    ];
  };

}
