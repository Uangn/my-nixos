{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.theming.systemTheme.enable =
      lib.mkEnableOption "theming systemTheme";
  };

  config = lib.mkIf config.homeMods.theming.systemTheme.enable {
    # dconf.settings = {
    #   "org/gnome/desktop/interface" = {
    #     color-scheme = "prefer-dark";
    #   };
    # };

    gtk = {
      enable = true;
      theme = {
        name = "Adwaita-dark";
        package = pkgs.gnome.gnome-themes-extra;
      };
    };

    # Wayland, X, etc. support for session vars
    # systemd.user.sessionVariables = ;

    qt = {
      enable = true;
      style.name = "adwaita-dark";
    };
  };

}
