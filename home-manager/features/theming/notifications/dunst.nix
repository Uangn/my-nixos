{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    homeMods.theming.notifications.dunst.enable =
      lib.mkEnableOption "theming notifications dunst";
  };

  config = lib.mkIf config.homeMods.theming.notifications.dunst.enable {
    services.dunst = {
      enable = true;
      settings = {
        global = {
          monitor = 0;
          follow = "none";
          width = 500;
          height = 300;
          origin = "bottom-right";
          offset = "10x50";
          notification_limit = 0;
          icon_corner_radius = 4;
          corner_radius = 4;
          transparency = 5;

          # Defines width in pixels of frame around the notification window.
          frame_width = 2;
          # Defines color of the frame around the notification window.
          # frame_color = "#aaaaaa"
          gap_size = 0;

          # separator_color = frame;

          # Sort messages by urgency.
          sort = "yes";
          idle_threshold = 120;

          ### Text ###
          font = "FiraCode NerdFont 16";

          # The spacing between lines.  If the height is smaller than the
          # font height, it will get raised to the font height.
          line_height = 0;

          markup = "full";
          format = "<b>%s</b>\\n%b\\n%p";

          alignment = "left";
          vertical_alignment = "center";

          # Show age of message if message is older than show_age_threshold
          # seconds.
          # Set to -1 to disable.
          show_age_threshold = 60;

          # Specify where to make an ellipsis in long lines.
          # Possible values are "start", "middle" and "end".
          ellipsize = "end";

          # Ignore newlines '\n' in notifications.
          ignore_newline = "no";

          # Stack together notifications with the same content
          stack_duplicates = true;

          # Hide the count of stacked notifications with the same content
          hide_duplicate_count = false;

          # Display indicators for URLs (U) and actions (A).
          show_indicators = "yes";

          ### History ###

          # Should a notification popped up from history be sticky or timeout
          # as if it would normally do.
          sticky_history = "yes";

          # Maximum amount of notifications kept in history
          history_length = 100;

          ### Wayland ###
          # These settings are Wayland-specific. They have no effect when using X11

          # Uncomment this if you want to let notications appear under fullscreen
          # applications (default: overlay)
          # layer = top;

          # Set this to true to use X11 output on Wayland.
          force_xwayland = false;

          ### Legacy
          force_xinerama = false;

          ### mouse
          mouse_left_click = "close_current";
          mouse_middle_click = "do_action, close_current";
          mouse_right_click = "close_all";
        };


        global = {
          frame_color = "#89B4FA";
          separator_color = "frame";
        };

        urgency_low = {
          timeout = 20;
          background = "#1F1F28";
          foreground = "#DCD7BA";
        };

        urgency_normal = {
          timeout = 20;
          background = "#1F1F28";
          foreground = "#DCD7BA";
        };

        urgency_critical = {
          timeout = 0;
          background = "#1F1F28";
          foreground = "#DCD7BA";
          frame_color = "#C8C093";
        };

        # Color setups
          #timeout = 0
          # Icon for notifications with low urgency, uncomment to enable
          #default_icon = /path/to/icon
          # Icon for notifications with normal urgency, uncomment to enable
          #default_icon = /path/to/icon
          # Icon for notifications with critical urgency, uncomment to enable
          #default_icon = /path/to/icon
      };
    };
  };

}
