# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  # You can import other home-manager modules here
  imports = [
    # If you want to use modules your own flake exports (from modules/home-manager):
    # outputs.homeManagerModules.example

    # Or modules exported from other flakes (such as nix-colors):
    # inputs.nix-colors.homeManagerModules.default

    # You can also split up your configuration and import pieces of it here:
    # ./nvim.nix
    ./global
    ./features/desktop
    ./features/dev
    ./features/hardware
    ./features/media
    ./features/theming
  ];

  nixpkgs = {
    # You can add overlays here
    overlays = [
      # Add overlays your own flake exports (from overlays and pkgs dir):
      outputs.overlays.additions
      outputs.overlays.modifications
      outputs.overlays.unstable-packages

      # You can also add overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    ];
    # Configure your nixpkgs instance
    config = {
      # Disable if you don't want unfree packages
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = _: true;
    };
  };

  # services.redshift.enable;

  services.parcellite = {
    enable = true;
    package = pkgs.clipit;
  };

  services.flameshot.enable = true;

  services.easyeffects.enable = true;

  services.picom = {
    enable = true;

    # fade = true;
    # fadeDelta = 3;

    # shadow = true;

    # menuOpacity = 1.0;
    # activeOpacity = 1.0;
    # inactiveOpacity = 0.8;

    settings = {
      unredir-if-possible = true;
    };
  };

  homeMods.dev.editors.nvim.enable = true;

  homeMods.theming.systemTheme.enable = true;
  homeMods.theming.notifications.dunst.enable = true;

  homeMods.hardware.control.audio.enable = true;
  homeMods.hardware.control.brightness.enable = true;
  homeMods.hardware.peripheral.enable = true;
  homeMods.hardware.screens.enable = true;

  homeMods.media.editors.enable = true;
  homeMods.media.viewers.enable = true;

  homeMods.desktop.settings.xdg-mime.enable = true;


  # Add stuff for your user as you see fit:
  home.packages = with pkgs; [
    rofi
    vesktop
    virt-manager
    wine
    spotify
    xclip
  ];

}
