cd ~
nix-shell -p git home-manager
git clone https://gitlab.com/Uangn/my-nixos.git
cd ~/my-nixos
cp /etc/nixos/hardware-configuration.nix ~/my-nixos/nixos/hardware-configuration.nix
export NIX_CONFIG="experimental-features = nix-command flakes"
sudo nixos-rebuild switch --flake .#hostname
reboot
home-manager switch --flake .#username@hostname
