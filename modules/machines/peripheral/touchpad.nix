{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    hostMods.peripheral.touchpad.enable =
      lib.mkEnableOption "touchpad configuration";
  };

  config = lib.mkIf config.hostMods.peripheral.touchpad.enable {
    services.xserver.libinput = {
      touchpad = {
        disableWhileTyping = true;
        tapping = false;
        naturalScrolling = false;
      };
    };
  };

}
