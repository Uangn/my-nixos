{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  userConfig,
  ...
}: {

  options = {
    hostMods.peripheral.keymaps.enable =
      lib.mkEnableOption "my keyboard options";
  };

  config = lib.mkIf config.hostMods.peripheral.keymaps.enable {
    services.xserver.xkbOptions = "caps:escape";
    console.useXkbConfig = true;
  };

}
