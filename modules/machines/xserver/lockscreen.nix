# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {

  options = {
    hostMods.xserver.lockscreen.enable =
      lib.mkEnableOption "lockscreen";
  };

  config = lib.mkIf config.hostMods.xserver.lockscreen.enable {
    programs.xss-lock = {
      enable = true;
      lockerCommand = 
        "${pkgs.i3lock-fancy}/bin/i3lock-fancy";
    };
  };

}
