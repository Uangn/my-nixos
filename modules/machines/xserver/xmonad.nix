# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  stolen-assets,
  ...
}: {

  options = {
    hostMods.xserver.xmonad.enable =
      lib.mkEnableOption "xmonad";
  };

  config = lib.mkIf config.hostMods.xserver.xmonad.enable {

    environment.systemPackages = with pkgs; [
      haskellPackages.xmobar
    ];

    systemd.targets.hybrid-sleep.enable = true;

    services = {

      logind.extraConfig = ''
        IdleAction=hybrid-sleep
      '';
        # IdleActionSec=20s

      xserver = {
        enable = true;

        displayManager = {
          lightdm = {
            enable = true;
            background = "${stolen-assets}/Pathos.jpg";
          };
        };

        windowManager = {
          xmonad = {
            enable = true;
            enableContribAndExtras = true;
          };
        };
      };
    };

  };

}
