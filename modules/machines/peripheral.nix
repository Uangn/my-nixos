{ pkgs, lib, ... }: {
  imports = [
    ./peripheral/touchpad.nix
    ./peripheral/keymaps.nix
  ];
}
