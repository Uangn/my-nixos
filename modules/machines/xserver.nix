{ pkgs, lib, ... }: {
  imports = [
    ./xserver/xmonad.nix
    ./xserver/lockscreen.nix
  ];
}
