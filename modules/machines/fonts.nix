# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)
{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {

  options = {
    hostMods.fonts.enable =
      lib.mkEnableOption "my font config";
  };

  config = lib.mkIf config.hostMods.fonts.enable {
    environment.systemPackages = with pkgs; [
      font-awesome
    ];

    fonts = {
      packages = with pkgs; [
        font-awesome
        (nerdfonts.override { fonts = [ "FiraCode" ]; })
      ];

      fontconfig = {
        enable = true;
        defaultFonts = {
          monospace = ["FiraCode NerdFont Regular"];
          sansSerif = ["FiraCode NerdFont Regular"];
        };
      };
    };

    console = {
      earlySetup = true;
      packages = with pkgs; [ terminus_font ];
      font = "ter-v32n";
    };
  };

}
